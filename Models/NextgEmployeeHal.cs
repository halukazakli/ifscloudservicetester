﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IFSCloudServiceTester.Models
{
    public class NextgEmployeeHal : OdataBase
    {
        public string EmplyeeId { get; set; }
        
        public string Name { get; set; }
        
        public string Status { get; set; }
        
        public string Username { get; set; }
        
        public string Password { get; set; }
        
        public string Role { get; set; }
    }
}
