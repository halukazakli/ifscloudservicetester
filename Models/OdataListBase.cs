﻿using System.Collections.Generic;

namespace IFSCloudServiceTester.Models
{
    public class OdataListBase<T> : IOdataList<T> where T : IOdata
    {
        public string Context { get; set; }
        
        public IEnumerable<T> Value { get; set; }
    }
}
