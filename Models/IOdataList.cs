﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace IFSCloudServiceTester.Models
{
    public interface IOdataList<T> where T : IOdata
    {
        [JsonProperty("@odata.context")]
        string Context { get; set; }

        IEnumerable<T> Value { get; set; }
    }
}
