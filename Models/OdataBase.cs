﻿namespace IFSCloudServiceTester.Models
{
    public class OdataBase : IOdata
    {
        public string Context { get; set; }

        public string Etag { get; set; }
        
        public string LuName { get; set; }
        
        public string KeyRef { get; set; }
        
        public string Objgrants { get; set; }
    }
}
