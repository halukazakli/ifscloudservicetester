﻿using Newtonsoft.Json;

namespace IFSCloudServiceTester.Models
{
    public interface IOdata
    {
        [JsonProperty("@odata.context")]
        public string Context { get; set; }

        [JsonProperty("@odata.etag")]
        string Etag { get; set; }

        string LuName { get; set; }

        string KeyRef { get; set; }

        string Objgrants { get; set; }
    }
}
