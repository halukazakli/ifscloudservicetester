﻿using System;

namespace IFSCloudServiceTester.Models
{
    public class NextgEmployeeExpenseHal : OdataBase
    {
        public int ExpenseId { get; set; }

        public string EmployeeId { get; set; }

        public DateTime ExpenseDate { get; set; }

        public string Period { get; set; }

        public string CustomerId { get; set; }

        public string ExpenseCode { get; set; }

        public string Description { get; set; }

        public string DocumentType { get; set; }
        
        public string DocumentNo { get; set; }

        public string Currency { get; set; }

        public decimal Amount { get; set; }
    }
}
