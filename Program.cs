﻿using Newtonsoft.Json.Linq;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace IFSCloudServiceTester
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                string tokenEndpoint = "https://pame8xi-dev1.build.ifs.cloud/auth/realms/pame8xidev1/protocol/openid-connect/token";
                string clientId = "??????";
                string clientSecret = "??????";



                // LOGIN & GET USER INFO

                string username = "??????";
                string password = "??????";

                string token = GetToken(tokenEndpoint, clientId, clientSecret).Result;
                var user = Login(username, password, token).Result;

                Console.WriteLine("Giriş yapıldı.");
                Console.WriteLine("Ad: " + user.Name);



                // GET EXPENSE LIST

                string period = "2023-07";

                var myExpenses = GetEmployeeExpenses(user.EmplyeeId, period, token).Result;

                foreach (var item in myExpenses)
                {
                    Console.WriteLine("ExpenseId: " + item.ExpenseId + " ExpenseCode: " + item.ExpenseCode);
                }



                //  POST NEW EXPENSE

                var postedExpenseInfo = PostEmployeeExpense(new Models.NextgEmployeeExpenseHal()
                {
                    EmployeeId = user.EmplyeeId,
                    ExpenseDate = new DateTime(2023, 7, 15),
                    CustomerId = "C0001",
                    ExpenseCode = "01",
                    Description = "Posted Expense",
                    DocumentType = "Invoice",
                    DocumentNo = "FAT202300000001",
                    Currency = "TRY",
                    Amount = 100
                }, token).Result;

                Console.WriteLine("Posted ExpenseId: " + postedExpenseInfo.ExpenseId);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            
        }



        static async Task<string> GetToken(string tokenEndpointi, string clientId, string clientSecret)
        {
            var data = new[]
            {
                new KeyValuePair<string, string>("client_id", clientId),
                new KeyValuePair<string, string>("client_secret", clientSecret),
                new KeyValuePair<string, string>("grant_type", "client_credentials"),
            };

            var content = new FormUrlEncodedContent(data);

            using HttpClient httpClient = new HttpClient();
            var response = await httpClient.PostAsync(tokenEndpointi, content);

            response.EnsureSuccessStatusCode();

            var jsonResponse = await response.Content.ReadAsStringAsync();

            dynamic obj = JObject.Parse(jsonResponse);

            return obj.access_token; 
        }

        static async Task<Models.NextgEmployeeHal> Login(string username, string password, string token)
        {
            string url = $"https://pame8xi-dev1.build.ifs.cloud/main/ifsapplications/projection/v1/NextgEmployeeHalHandling.svc/NextgEmployeeHalSet?$filter=Username eq '{username}' and Password eq '{password}'";

            using HttpClient httpClient = new HttpClient();

            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            
            var response = await httpClient.GetAsync(url);

            response.EnsureSuccessStatusCode();

            var jsonResponse = await response.Content.ReadAsStringAsync();

            var result = Newtonsoft.Json.JsonConvert.DeserializeObject<Models.OdataListBase<Models.NextgEmployeeHal>>(jsonResponse);

            var list = result.Value.ToList();

            if (list.Count == 0)
                throw new Exception("Kullanıcı veya şifre hatalı!");

            if (list[0].Status != "Active")
                throw new Exception("Hesabınız aktif değildir!");

            if (list[0].Role != "Employee")
                throw new Exception("Sadece çalışanlar giriş yapabilir!");

            return list[0];
        }

        static async Task<IEnumerable<Models.NextgEmployeeExpenseHal>> GetEmployeeExpenses(string employeeId, string period, string token)
        {
            string url = $"https://pame8xi-dev1.build.ifs.cloud/main/ifsapplications/projection/v1/NextgEmployeeExpenseHalHandling.svc/NextgEmployeeExpenseHalSet?$filter=EmployeeId eq '{employeeId}' and Period eq '{period}'&$orderby=ExpenseDate";

            using HttpClient httpClient = new HttpClient();

            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

            var response = await httpClient.GetAsync(url);

            response.EnsureSuccessStatusCode();

            var jsonResponse = await response.Content.ReadAsStringAsync();

            var result = Newtonsoft.Json.JsonConvert.DeserializeObject<Models.OdataListBase<Models.NextgEmployeeExpenseHal>>(jsonResponse);

            return result.Value;
        }

        static async Task<Models.NextgEmployeeExpenseHal> PostEmployeeExpense(Models.NextgEmployeeExpenseHal expense, string token)
        {
            string url = $"https://pame8xi-dev1.build.ifs.cloud/main/ifsapplications/projection/v1/NextgEmployeeExpenseHalHandling.svc/NextgEmployeeExpenseHalSet";

            using HttpClient httpClient = new HttpClient();

            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

            // -------------------------------------------------------------

            var options = new Newtonsoft.Json.JsonSerializerSettings
            {
                NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore,
                DateFormatString = "yyyy-MM-dd"
            };

            var jsonContent = Newtonsoft.Json.JsonConvert.SerializeObject(expense, options);
            var buffer = System.Text.Encoding.UTF8.GetBytes(jsonContent);
            var byteContent = new ByteArrayContent(buffer);
            byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            // -------------------------------------------------------------

            var response = await httpClient.PostAsync(url, byteContent);

            response.EnsureSuccessStatusCode();

            var jsonResponse = await response.Content.ReadAsStringAsync();

            var result = Newtonsoft.Json.JsonConvert.DeserializeObject<Models.NextgEmployeeExpenseHal>(jsonResponse);

            return result;
        }
    }
}
